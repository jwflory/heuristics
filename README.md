heuristics
==========

[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)
[![Build Status](https://travis-ci.org/jwflory/heuristics.svg?branch=master)](https://travis-ci.org/jwflory/heuristics)

"Heuristics is the science of finding new ways to solve problems" (Abbott 2004, 81)


## About

This repository is a collection of various information about things that are important to me.
At the time of initial commit, I mostly intend to publish topics about Free and Open Source Software here, but the scope may change over time.
Think of it as a mindmap of data and information I reference often.


## Legal

Licensed under [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).
Not a lawyer?
This basically means you are free to:

* **Share**:
  Copy and redistribute the material in any medium or format
* **Adapt**:
  Remix, transform, and build upon the material for any purpose, even commercially

Provided you meet the following terms:

* **Attribution**:
  You must give appropriate credit, provide a link to the license, and indicate if changes were made.
  You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
* **ShareAlike**:
  If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
* **No additional restrictions**:
  You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
